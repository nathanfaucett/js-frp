var Stream = require("./Stream");


var frp = exports;


frp.Stream = Stream;
frp.stream = Stream.create;
frp.reduce = Stream.reduce;
frp.merge = Stream.merge;