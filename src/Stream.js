var apply = require("@nathanfaucett/apply"),
    isArray = require("@nathanfaucett/is_array"),
    indexOf = require("@nathanfaucett/index_of"),
    arrayForEach = require("@nathanfaucett/array-for_each"),
    isFunction = require("@nathanfaucett/is_function");


var StreamPrototype;


module.exports = Stream;


function Stream() {
    this._listeners = [];
}
StreamPrototype = Stream.prototype;

function createStream(listener, value) {
    var stream = new Stream();

    if (isFunction(listener)) {
        stream.on(listener);
    }
    if (arguments.length === 2) {
        stream.update(value);
    }

    return stream;
}

function reduceStream(stream, reducer, initalValue) {
    var newStream, reducedValue;

    if (!isFunction(reducer)) {
        throw new TypeError("Stream.reduce(stream: Stream, reducer: Function[, initalValue: Any]) Invalid reducer type " + reducer);
    }

    newStream = new Stream();
    reducedValue = initalValue;

    stream.on(function onStream(streamValue) {
        reducedValue = reducer(reducedValue, streamValue);
        newStream.update(reducedValue);
    });

    return newStream;
}

function mergeStreams(streams, listener) {
    var newStream, streamData;

    if (!isArray(streams)) {
        throw new TypeError("Stream.reduce(streams: Array<Stream>, listener: Function) Invalid streams type " + streams);
    }
    if (!isFunction(listener)) {
        throw new TypeError("Stream.reduce(streams: Array<Stream>, listener: Function) Invalid listener type " + listener);
    }

    newStream = new Stream();
    streamData = new Array(streams.length);

    arrayForEach(streams, function each(stream, index) {
        stream.on(function onStream(value) {
            streamData[index] = value;
            newStream.update(apply(listener, streamData));
        });
    });

    return newStream;
}

Stream.create = createStream;
Stream.reduce = reduceStream;
Stream.merge = mergeStreams;

StreamPrototype.on = function(listener) {
    var listeners, index;

    if (!isFunction(listener)) {
        throw new TypeError("Stream.on(listener: Function) Invalid listener type " + listener);
    }

    listeners = this._listeners;
    index = indexOf(listeners, listener);

    if (index === -1) {
        listeners.push(listener);
    }

    return this;
};

StreamPrototype.off = function(listener) {
    var listeners = this._listeners,
        index = indexOf(listeners, listener);

    if (index !== -1) {
        listeners.splice(index, 1);
    }

    return this;
};

StreamPrototype.update = function(value) {
    var listeners = this._listeners,
        i = -1,
        il = listeners.length - 1;

    while (i++ < il) {
        listeners[i](value);
    }

    return this;
};

StreamPrototype.throttle = function(ms, callback) {
    var _this = this,
        updates = [];

    function throttle(value) {
        updates.push(value);
    }

    this.on(throttle);

    setTimeout(function onThrottle() {
        callback(updates);
        _this.off(throttle);
    }, ms);
};