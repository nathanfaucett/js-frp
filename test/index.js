var tape = require("tape"),
    arrayForEach = require("@nathanfaucett/array-for_each"),
    frp = require("..");


tape("frp.stream/frp.reduce", function(assert) {
    var EVENTS = ['a', 'b', 'c', 'd'],
        INDEX = 0,

        actionStream = frp.stream(),
        countStream = frp.reduce(actionStream, function countReducer(value, streamValue) {
            assert.equals(EVENTS[INDEX], streamValue);
            return value + 1;
        }, 0);

    countStream.on(function onCount(value) {
        assert.equals(INDEX + 1, value);
    });

    arrayForEach(EVENTS, function each(event) {
        actionStream.update(event);
        INDEX += 1;
    });

    assert.end();
});

tape("frp.merge", function(assert) {
    var MOUSE_EVENTS = ["up", "up", "down", "down"],
        MOUSE_INDEX = 0,
        KEY_EVENTS = ['a', 'b', 'c', 'd'],
        KEY_INDEX = -1,

        mouseStream = frp.stream(),
        keyStream = frp.stream(),

        i, il;

    frp.merge([mouseStream, keyStream], function onUpdate(mouseValue, keyValue) {

        assert.equals(MOUSE_EVENTS[MOUSE_INDEX], mouseValue);
        assert.equals(KEY_EVENTS[KEY_INDEX], keyValue);

        if (KEY_INDEX === 3) {
            assert.end();
        }
    });

    for (i = 0, il = 4; i < il; i++) {
        mouseStream.update(MOUSE_EVENTS[i]);
        KEY_INDEX += 1;
        keyStream.update(KEY_EVENTS[i]);
        MOUSE_INDEX += 1;
    }
});

tape("Stream.throttle", function(assert) {
    var EVENTS = ['a', 'b', 'c', 'd'],
        actionStream = frp.stream();

    actionStream.throttle(100, function onThrottled(updates) {
        assert.deepEquals(EVENTS, updates);
        assert.end();
    });

    arrayForEach(EVENTS, function each(event) {
        setTimeout(function onTimeout() {
            actionStream.update(event);
        }, 10);
    });
});